%%%-------------------------------------------------------------------
%% @doc mysqerl top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(acmysql_sup).

-behaviour(supervisor).

%% API
-export([start_link/0, create_db_pools/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
  create_db_pools(),
  {ok, { {one_for_all, 0, 1}, []} }.

%%====================================================================
%% Internal functions
%%====================================================================
create_pool(CfgName, CfgMap = #{database := _}) ->
  Host = maps:get(host, CfgMap, "localhost"),
  User = maps:get(user, CfgMap, "remote"),
  Passwd = maps:get(password, CfgMap, "remote"),
  Db = maps:get(database, CfgMap),
  MaxConn = maps:get(max_connections, CfgMap, 10),
  InitConn = maps:get(init_connections, CfgMap, max(MaxConn, ceil(MaxConn / 3))),
  GroupName = maps:get(group, CfgMap, CfgName),

  PoolConfig = [{name, CfgName},
              {group, GroupName},
              {max_count, MaxConn},
              {init_count, InitConn},
              {start_mfa, {acmysql_conn, start_link, [CfgName, Host, User, Passwd, Db]}}],
  pooler:new_pool(PoolConfig);

create_pool(_CfgName, _CfgMap) -> ok.

create_db_pools() ->
  Cfgs = application:get_all_env(acmysql),
  lists:foreach(fun({CfgName, Cfg}) -> create_pool(CfgName, maps:from_list(Cfg)) end, Cfgs).
