%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. Apr 2018 11:11 AM
%%%-------------------------------------------------------------------
-module(acmysql_conn).
-author("adean").

-behaviour(gen_server).

%% API
-export([start_link/5, start_link/6]).
-export([get_records/4, insert_record/3, update_record/4, delete_records/3, get_records_raw/3,
          upsert_record/4]).

-define(CFG(Key), {Key, maps:get(Key, Cfg)}).
-define(B(K), erlang:atom_to_binary(K, utf8)).
-define(TIMEOUT, 10000).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {name, conn, timeout}).

%%%===================================================================
%%% API
%%%===================================================================
start_link(Name, Host, User, Password, Db, Timeout) ->
  gen_server:start_link(?MODULE, [Name, Host, User, Password, Db, Timeout], []).

start_link(Name, Host, User, Password, Db) ->
  start_link(Name, Host, User, Password, Db, ?TIMEOUT).

get_records(Pool, Table, Fields, Where) ->
  in_transaction(fun(Pid) ->
    case gen_server:call(Pid, {query, Table, Fields, Where}) of
      {ok, Recs} -> {ok, Recs};
      _ -> {error, []}
    end
  end, Pool).

get_records_raw(Pool, Query, Data) ->
  in_transaction(fun(Pid) ->
    lager:debug("This is the pid = ~p", [Pid]),
    case gen_server:call(Pid, {query_raw, Query, Data}) of
      {ok, Recs} -> {ok, Recs};
      _ -> {error, []}
    end
  end, Pool).

insert_record(Pool, Table, DMap = #{}) ->
  in_transaction(fun(Pid) ->
                  %lager:debug("In transaction:insert_record"),
                  gen_server:cast(Pid, {insert, Table, DMap})
                 end, Pool).

upsert_record(Pool, Table, DMap = #{}, UMap = #{}) ->
  in_transaction(fun(Pid) ->
                  %lager:debug("In transaction:insert_record"),
                  gen_server:cast(Pid, {upsert, Table, DMap, UMap})
                 end, Pool).

update_record(Pool, Table, DMap = #{}, Where) ->
  in_transaction(fun(Pid) ->
                     gen_server:cast(Pid, {update, Table, DMap, Where})
                 end, Pool).

delete_records(Pool, Table, Where) ->
  in_transaction(fun(Pid) ->
                     gen_server:cast(Pid, {delete, Table, Where})
                 end, Pool).

in_transaction(Func, PoolName) ->
  lager:debug(">>>>>>> Trying to get a Pool Member from ~p", [PoolName]),
  case pooler:take_group_member(PoolName) of
    Pid when is_pid(Pid)->
      try Func(Pid) of
        Result ->
          pooler:return_group_member(PoolName, Pid, ok),
          Result
      catch
        Exception:Reason ->
          lager:error("in_transaction_catch: ~p - ~p", [Exception, Reason]),
          lager:error("~p", [erlang:get_stacktrace()]),
          pooler:return_group_member(PoolName, Pid, fail),
          {error, []}
      end;
    error_no_members  ->
      lager:error("Pooler Error: No Members for Pool ~p, increase max connections", [PoolName]),
      {error, []};
    {error_no_group, Pool} ->
      lager:error("Invalid Pool/Group ~p", [Pool]),
      {error, []}
  end.

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([Name, Host, User, Password, Db, Timeout]) ->
  lager:info("Adding DB Connection for ~s.....", [Name]),
  {ok, Conn} = mysql:start_link([{host, Host}, {user, User}, {password, Password}, {database, Db}]),
  {ok, #state{name = Name, conn = Conn, timeout = Timeout}}.

handle_call({query, Table, Fields, Data, Timeout}, _From, State = #state{conn = Conn, timeout = Timeout}) ->
  Result = query(Conn, Table, Fields, Data, Timeout),
  {reply, Result, State};
handle_call({query, Table, Fields, Data}, _From, State = #state{conn = Conn, timeout = Timeout}) ->
  Result = query(Conn, Table, Fields, Data, Timeout),
  {reply, Result, State};

handle_call({query_raw, Query, Data}, _From, State = #state{conn = Conn, timeout = Timeout}) ->
  Result = query_raw(Conn, Query, Data, Timeout),
  {reply, Result, State};

handle_call({insert, Table, DMap}, _From, State = #state{conn = Conn}) ->
  insert(Conn, Table, DMap),
  {reply, ok, State};

handle_call({update, Table, DMap, Where}, _From, State = #state{conn = Conn}) ->
  update(Conn, Table, DMap, Where),
  {reply, ok, State};

handle_call({delete, Table, Where}, _From, State = #state{conn = Conn}) ->
  delete(Conn, Table, Where),
  {reply, ok, State};

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

handle_cast({insert, Table, DMap}, State = #state{conn = Conn}) ->
  insert(Conn, Table, DMap),
  {noreply, State};

handle_cast({upsert, Table, DMap, UMap}, State = #state{conn = Conn}) ->
  insert_or_update(Conn, Table, DMap, UMap),
  {noreply, State};

handle_cast({update, Table, DMap, Where}, State = #state{conn = Conn}) ->
  update(Conn, Table, DMap, Where),
  {noreply, State};

handle_cast({delete, Table, Where}, State = #state{conn = Conn}) ->
  delete(Conn, Table, Where),
  {noreply, State};

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%query(Pid, Table, Fields, Where) -> query(Pid, Table, Fields, Where, ?TIMEOUT).
query(Pid, Table, Fields, Where, Timeout) ->
  FieldsBin = lists:join(",", Fields),
  [WFieldsBin, WValues] = get_where(Where),

  Query = [<<"SELECT ">>, FieldsBin, <<" FROM ">>, Table, <<" WHERE ">>, WFieldsBin],
  case mysql:query(Pid, Query, WValues, Timeout) of
    {ok, ColumnNames, Rows} ->
      %lager:debug(">>>>>>>>> Got Records Back ~p, ~p", [ColumnNames, Rows]),
      Recs = lists:map(fun(R) -> maps:from_list(lists:zip(ColumnNames, R)) end, Rows),
      %lager:debug("Zipped! ~p", [Recs]),
      {ok, Recs};
    Error ->
      lager:error("mysql_conn: ~p", [Error]),
      {error, []}
  end.

query_raw(Pid, Query, Data, Timeout) ->
  case mysql:query(Pid, Query, Data, Timeout) of
    {ok, ColumnNames, Rows} ->
      Recs = lists:map(fun(R) -> maps:from_list(lists:zip(ColumnNames, R)) end, Rows),
      {ok, Recs};
    Result ->
      lager:error("mysql_conn:  ~p",[Result]),
      {error, []}
  end.

insert(Pid, Table, DMap) when is_map(DMap) ->
  %lager:debug("mysql-conn: Inserting -> ~p to ~s", [DMap, Table]),
  insert(Pid, Table, maps:keys(DMap), maps:values(DMap)).

insert(Pid, Table, ColumnNames, Values) when is_list(Values) ->
  Fields = lists:join(",", [ convert(X) || X <- ColumnNames]),
  FieldPos = lists:join(",", ["?" || _ <- ColumnNames]),
  Query = binary:list_to_bin(
            [<<"INSERT INTO ">>, convert(Table), <<" (">>, Fields, <<") VALUES (">>, FieldPos, <<")">>]
           ),
  lager:debug("~p", [Query]),
  case mysql:query(Pid, Query, Values) of
    ok -> {ok, mysql:insert_id(Pid)};
    Error  ->
      lager:error("Error Inserting Record: ~p", [Error]),
      {error, Error}
  end.

-spec insert_or_update(pid(), atom(), map(), map()) -> {ok, integer()} | {error, _}.
insert_or_update(Pid, Table, RecMap, Update) when is_map(RecMap) ->
  ColumnNames = maps:keys(RecMap),
  Values1 = maps:values(RecMap),
  Fields = lists:join(",", [ convert(X) || X <- ColumnNames]),
  FieldPos = lists:join(",", ["?" || _ <- ColumnNames]),

  %<<==== update stuff =====
  {TFields, Values2} = lists:foldl(fun({F,V}, {WFlds, WVar}) ->
                            {[[convert(F), " = ? "] | WFlds], [V | WVar]}
                        end,
                        {[], []}, maps:to_list(Update)),
  WFields = lists:join(",", TFields),
  WValues = Values1 ++ Values2,
  %===== where stuff =====>>>
  Query = binary:list_to_bin(
            [<<"INSERT INTO ">>, convert(Table), <<" (">>, Fields, <<") VALUES (">>, FieldPos,
              <<") ON DUPLICATE KEY UPDATE ">>, WFields]
           ),
  lager:debug("~p , ~p", [Query, WValues]),
  case mysql:query(Pid, Query, WValues) of
    ok -> {ok, mysql:insert_id(Pid)};
    Error  ->
      lager:error("Error Inserting Record: ~p", [Error]),
      {error, Error}
  end.

-spec update(Pid :: pid(), Table :: binary(), Rec :: map(), Where :: list()) -> {ok, integer()} | {error, 0}.
update(Pid, Table, Rec, Where) when is_list(Where) and is_map(Rec) ->
  {TFields, TValues} = get_set_fields(Rec),
  {WFields, Values} = get_where(Where, TValues),
  Query = binary:list_to_bin(
            [<<"UPDATE ">>, convert(Table), <<" set ">>, lists:reverse(TFields), <<" where 1=1 AND ">>, lists:reverse(WFields)]
           ),
  lager:debug("~s - ~p ", [Query, Values]),
  case mysql:query(Pid, Query, Values) of
    ok -> {ok, mysql:affected_rows(Pid)};
    Error  ->
      lager:error("Error Updating Record: ~p", [Error]),
      {error, 0}
  end.

delete(Pid, Table, Where) ->
  {WFields, WValues} = get_where(Where),

  Query = [<<"DELETE from ">>, convert(Table), <<" where 1=1 AND ">>, lists:reverse(WFields)],
  lager:debug("~s - ~p ", [Query, WValues]),
  case mysql:query(Pid, Query, WValues) of
    ok -> {ok, mysql:affected_rows(Pid)};
    _  -> {error, 0}
  end.

% ========== internal functions ========

transform_in({X, is, null}) -> {[ convert(X), <<" is null ">> ]};
transform_in({X, 'not', null}) -> {[ convert(X), <<" is not null ">> ]};
transform_in({X, eq, Y}) -> {[ convert(X), <<" = ? ">> ], Y};
transform_in({X, ne, Y}) -> {[ convert(X), <<" <> ? ">> ], Y};
transform_in({X, gte, Y}) -> {[ convert(X), <<" >= ? ">> ], Y};
transform_in({X, lte, Y}) -> {[ convert(X), <<" <= ? ">> ], Y};
transform_in({X, gt, Y}) -> {[ convert(X), <<" > ? ">> ], Y};
transform_in({X, lt, Y}) -> {[ convert(X), <<" < ? ">> ], Y}.

convert(X) when is_atom(X) -> erlang:atom_to_binary(X, latin1);
convert(X) when is_list(X) -> binary:list_to_bin(X);
convert(X) -> X.


get_set_fields(Rec) ->
  {TFields, TValues} = lists:foldl(fun({F,V}, {WFlds, WVar}) ->
                            {[[convert(F), " = ? "] | WFlds], [V | WVar]}
                        end,
                        {[], []}, maps:to_list(Rec)),
  {lists:join(",", TFields), TValues}.

get_where(Where) ->
  get_where(Where, []).

get_where(Where, TValues) ->

  {TWFields, WValues} = lists:foldl(fun(FV, {WFlds, WVar}) ->
                            {W, V} = transform_in(FV),
                            {[W | WFlds], [V | WVar]}
                        end,
                        {[], TValues}, Where),
  WFields = lists:join(" AND ", TWFields),
  Values = lists:reverse(WValues),
  {WFields, Values}.

%% Separate calls to fetch more info about the last query
%LastInsertId = mysql:insert_id(Pid),
%AffectedRows = mysql:affected_rows(Pid),
%WarningCount = mysql:warning_count(Pid),

%%% Mnesia style transaction (nestable)
%Result = mysql:transaction(Pid, fun () ->
    %ok = mysql:query(Pid, "INSERT INTO mytable (foo) VALUES (1)"),
    %throw(foo),
    %ok = mysql:query(Pid, "INSERT INTO mytable (foo) VALUES (1)")
%end),
%case Result of
    %{atomic, ResultOfFun} ->
        %io:format("Inserted 2 rows.~n");
    %{aborted, Reason} ->
        %io:format("Inserted 0 rows.~n")
%end

%% Multiple queries and multiple result sets
%{ok, [{[<<"foo">>], [[42]]}, {[<<"bar">>], [[<<"baz">>]]}]} =
    %mysql:query(Pid, "SELECT 42 AS foo; SELECT 'baz' AS bar;"),

%%% Graceful timeout handling: SLEEP() returns 1 when interrupted
%{ok, [<<"SLEEP(5)">>], [[1]]} =
    %mysql:query(Pid, <<"SELECT SLEEP(5)">>, 1000),

